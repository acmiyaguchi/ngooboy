"""CPU module emulates the modified Z80 processor built into the gameboy.
This module provides the fetch, decode, and execute cycles."""
from ngooboy import mmu, registers


class CPU(object):
    """CPU class to run instructions"""
    def __init__(self):
        """Create and initialize all the registers"""
        self.clock = {'m': 0, 't': 0}
        self.mmu = mmu.MMU()
        self.reg = registers.Register()
        self.pc = 0

    def reset(self):
        """Reset the cpu by zeroing out all registers"""
        for key in self.clock:
            self.clock[key] = 0
        self.pc = 0
        self.reg.reset()

    def dispatch(self, opcode):
        pass

    def _ld8_rr(self, destination, source):
        """Load value of one register into another"""
        self.reg[destination] = self.reg[source]

    def _ld8_rm(self, destination, source):
        """Load value in memory to destination"""
        value = self.mmu.read_byte(self.reg[source])
        self.reg[destination] = value

    def _ld8_mr(self, destination, source):
        """Load value of register into memory address"""
        self.mmu.write_byte(self.reg[destination], self.reg[source])

    def _ld8_ri(self, destination, source):
        """Load 8 bits from immediate to register"""
        self.pc += 1
        self.reg[destination] = self.mmu.read_byte(self.pc)

    def _ld16_ri(self, destination, source):
        """Load 16 bits from immediate to register"""
        self.pc += 1


    def _ld16_mr(self, destination, source):
        #TODO
        pass

    def _nop(self):
        #TODO
        pass
