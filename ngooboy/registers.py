""" Provides an interface to the registers on the gameboy cpu """


class Registers(object):
    FLAGS = {'z': 7, 'n': 6, 'h': 5, 'c': 4}

    def __init__(self):
        self.reg = {
                'a': 0, 'f': 0,
                'b': 0, 'c': 0,
                'd': 0, 'e': 0,
                'h': 0, 'l': 0,
                'sp': 0
                }

    def reset(self):
        for key in self.reg:
            self.reg[key] = 0

    def read_byte(self, reg):
        return self.reg[reg] & 0xFF

    def read_word(self, reg):
        upper_byte = self.reg[reg[0]]
        lower_byte = self.reg[reg[1]]
        return ((upper_byte & 0xFF)  << 8) | (lower_byte & 0xFF)

    def write_byte(self, reg, val):
        self.reg[reg] = val

    def write_word(self, reg, val):
        self.reg[reg[0]] = (val >> 8) & 0xFF
        self.reg[reg[1]] = val & 0xFF

    def set_flag(self, flag, bit):
        set_byte = self.reg['f'] ^ (1 << self.FLAGS[flag])
        self.reg['f'] = set_byte

