try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
    config = {
            'description': 'A python gameboy emulator',
            'author': 'Anthony Miyaguchi',
            'url': 'https://github.com/acmiyaguchi/ngooboy',
            'download_url': 'https://github.com/acmiyaguchi/ngooboy.',
            'author_email': 'acmiyaguchi@gmail.com',
            'version': '0.1',
            'install_requires': ['nose'],
            'packages': ['ngooboy'],
            'scripts': [],
            'test_suite': 'nose.collector',
            'name': 'ngooboy'
            }

    setup(**config)
